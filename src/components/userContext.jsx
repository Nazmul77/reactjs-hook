import React, { Component, createContext } from 'react'

let Context =null;
const {Provider, Consumer}=(Context= createContext())
 class UserProvider extends Component {
    state={
        user: {
            name: 'Muhammad Nazmul Hossain',
            email: 'mdnazmulhossainnn@gmail.com',
            language:[
                'C Programming',
                'Java',
                "JavaScipt",
                "Python",

            ]

        },
        isAuthenticated: true
    }
                login=()=>{
                    this.setState({isAuthenticated:true})
                }
                logOut=()=>{
                    this.setState({isAuthenticated:false})
                }
                    addToShowcase=(item)=>{
                        const user={...this.state.user};
                        user.language.push(item);
                        this.setState({user});
                    }












    render() {
        return (
            <div>
                <Provider 
                value={{
                    ...this.state,
                    login: this.login,
                    logout: this.logOut
                }}
                >
                    {this.props.children}

                </Provider>
                
            </div>
        )
    }
}

export {UserProvider, Consumer as UserConsumer, Context as UserContext }